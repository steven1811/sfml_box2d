#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <iostream>
#include <memory>
#include "DebugDraw.h"
#include "SharedData.h"
#include "GUI/GUIMain.h"

#define SCALE 30.0f

using namespace std;

void CreateGround(b2World& World, float X, float Y)
{
	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(X / SCALE, Y / SCALE);
	BodyDef.type = b2_staticBody;
	b2Body* Body = World.CreateBody(&BodyDef);
	b2PolygonShape Shape;
	Shape.SetAsBox((800.f / 2) / SCALE, (16.f / 2) / SCALE);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 0.f;
	FixtureDef.friction = 1.6f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);
}

void CreateBox(b2World& World, int MouseX, int MouseY)
{
	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(MouseX / SCALE, MouseY / SCALE);
	BodyDef.type = b2_dynamicBody;
	BodyDef.angularDamping=1.0f;
	b2Body* Body = World.CreateBody(&BodyDef);
	b2PolygonShape Shape;
	Shape.SetAsBox((32.f / 2) / SCALE, (32.f / 2) / SCALE);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 10.f;
	FixtureDef.friction = 0.7f;
	FixtureDef.restitution = 0.5f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);
}

void CreateCircle(b2World& World, int MouseX, int MouseY) {
	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(MouseX / SCALE, MouseY / SCALE);
	BodyDef.type = b2_dynamicBody;
	b2Body* Body = World.CreateBody(&BodyDef);
	b2CircleShape Shape;
	Shape.m_radius = (32.0f / 2) / SCALE;
	b2FixtureDef FixtureDef;
	FixtureDef.density = 1.f;
	FixtureDef.friction = 0.7f;
	FixtureDef.restitution = 0.6f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);
}


int main() {
	shared_ptr<sf::RenderWindow> win(nullptr);
	shared_ptr<SharedData> global(nullptr);
	shared_ptr<b2World> world(nullptr);
	sf::Clock fpsClock;

	float lastTime = 0;
	bool gameRunning = true;
	win=make_shared<sf::RenderWindow>(sf::VideoMode(800, 600), "SFML + Box2D");
	
	b2Vec2 Gravity(0.f, 9.8f);
	world = make_shared<b2World>(Gravity);
	global = make_shared<SharedData>(win, world);

	GUIMain gui(*win);
	GUI::Button &btn=gui.addButton("Test", 100, 100);
	GUI::Button &btn2 = gui.addButton("AWDWAD", 300, 300);
	GUI::Button &btn3 = gui.addButton("erwer", 400, 400);
	GUI::Button &btn4 = gui.addButton("wer", 500, 500);

	CreateGround(*world, 400.f, 500.f);
	DebugDraw debugDrawer(global);
	debugDrawer.SetFlags(
		b2Draw::e_aabbBit +
		b2Draw::e_centerOfMassBit +
		b2Draw::e_jointBit +
		b2Draw::e_pairBit +
		b2Draw::e_shapeBit
	);
	world->SetDebugDraw(&debugDrawer);

	sf::Clock test;
	while (win->isOpen()) {
		//Meassure fps;
		float currentTime = fpsClock.restart().asSeconds();
		global->fps = 1.f / currentTime;
		lastTime = currentTime;
		
		//Poll Events
		sf::Event event;

		while (win->pollEvent(event))
		{
			gui.pollEvents(event);

			if (btn.isHighlighted()) {
				cout << "Highlit BTN1!" << endl;
			}

			if (btn2.isHighlighted()) {
				cout << "Highlit BTN2!" << endl;
			}

			if (btn3.isHighlighted()) {
				cout << "Highlit BTN3!" << endl;
			}

			if (btn4.isHighlighted()) {
				cout << "Highlit BTN4!" << endl;
			}

			if (btn.isPressed()) {
				cout << "Pressed BTN1!" << endl;
			}
			if (btn2.isPressed()) {
				cout << "Pressed BTN2!" << endl;
			}
			if (btn3.isPressed()) {
				cout << "Pressed BTN3!" << endl;
			}
			if (btn4.isPressed()) {
				cout << "Pressed BTN4!" << endl;
			}

			if (event.type == sf::Event::Closed)
				win->close();
			if (event.type == event.KeyPressed) {
				if (event.key.code == sf::Keyboard::F1) {
					debugDrawer.ToggleOverlay();
				}
				if (event.key.code == sf::Keyboard::F2) {
					gameRunning = !gameRunning;
				}
				if (event.key.code == sf::Keyboard::F3) {
					debugDrawer.ToggleConsole();
				}
				if (event.key.code == sf::Keyboard::A) {
					world->GetBodyList()[0].ApplyAngularImpulse(-0.5, true);
				}
				if (event.key.code == sf::Keyboard::D) {
					world->GetBodyList()[0].ApplyAngularImpulse(0.5, true);
				}
				if (event.key.code == sf::Keyboard::F6) {
					int test = rand();
					stringstream taron;
					taron << test;
					btn.setText(taron.str());
				}
				if (event.key.code == sf::Keyboard::F7) {
					int test = rand();
					stringstream taron;
					taron << test;
					btn3.setText(taron.str());
				}
			}
			if (event.type == event.MouseButtonPressed) {
				if (event.mouseButton.button == sf::Mouse::Left) {
					win->getDefaultView().getViewport().left;
					int MouseX = sf::Mouse::getPosition(*win).x;
					int MouseY = sf::Mouse::getPosition(*win).y;
					sf::Vector2f mapped = win->mapPixelToCoords(sf::Vector2i(MouseX, MouseY));
					CreateCircle(*world, (int) mapped.x, (int) mapped.y);
				}
			}
		}

	

		//Calculate Physics
		if (gameRunning) {
			world->Step(1 / global->fps, 1, 1);
		}
		
		//cout << "Bodies: " << world->GetBodyCount() << endl;

		//GUIMain stuff
		win->clear(sf::Color(180, 200, 255));
		world->DrawDebugData();

		debugDrawer.DrawOverlay();
		gui.draw();
		win->display();
	}
	return 0;
}