#pragma once
#include <SFML\Graphics.hpp>
#include <Box2D\Box2D.h>
#include <memory>

using namespace std;

class SharedData {
public:
	SharedData(shared_ptr<sf::RenderWindow> window, shared_ptr<b2World> world_) : win(window), world(world_) {
		mainFont.loadFromFile("data/arial.ttf");
	}
	sf::Font mainFont;
	float fps;
	shared_ptr<sf::RenderWindow> win;
	shared_ptr<b2World> world;
};