#pragma once
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <sstream>
#include "sharedData.h"
#include <stdbool.h>

#define SCALE 30.0f
#define overlayRefreshRate_ms 250

using namespace std;

class DebugDraw :virtual public b2Draw {
private:
	shared_ptr<SharedData> global;
	shared_ptr<sf::RenderWindow> win;
	shared_ptr<b2World> world;

	sf::CircleShape circle;
	sf::ConvexShape polygon;

	sf::Text fpsCounter;
	sf::Clock overlayRefreshClock;

	sf::Color convertToSFMLColor(const b2Color &color);

	bool overlayEnabled = false;
	bool consoleEnabled = false;
public:
	DebugDraw(shared_ptr<SharedData> globalData);
	virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
	virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
	virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
	virtual void DrawTransform(const b2Transform& xf);
	virtual void DrawPoint(const b2Vec2& p, float32 size, const b2Color& color);
	void DrawOverlay();
	void ToggleOverlay();
	void ToggleConsole();
};