#pragma once
#include <SFML\Graphics.hpp>
#include <iostream>
#include <memory>
#include <stdbool.h>
#include <list>
#include <GUI/Elements/Button.h>
#include <GUI/Views/VerticalView.h>
#include <SFML/Graphics.hpp>

#define GUI_FONT_PATH "data/guiFont.ttf"

	using namespace std;
	using namespace GUI;

	class GUIMain {

	private:
		list<Button> buttonList;
		list<VerticalView> verticalViewList;
		sf::RenderWindow &win;
		sf::Font guiFont;

	public:
		bool showSidebar = true;
		GUIMain(sf::RenderWindow &render) : win(render) {
			guiFont.loadFromFile(GUI_FONT_PATH);
		}

		Button& addButton(sf::String text, float posX, float posY);

		void pollEvents(sf::Event &event);
		void draw();

	};