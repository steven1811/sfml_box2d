#include <GUI/Common/Element.h>

using namespace GUI;

void Element::handleEvents(sf::Event &event) {
	if (!hidden) {
		eventFlags &= ~(1 << EVENT_BTN_PRESS);

		if (event.type == event.MouseMoved) {
			int MouseX = sf::Mouse::getPosition(win).x; //Change
			int MouseY = sf::Mouse::getPosition(win).y; //same
			sf::Vector2f mapped = win.mapPixelToCoords(sf::Vector2i(MouseX, MouseY));
			
			bool highlighted = mListenShape->getGlobalBounds().contains(mapped);
			if (highlighted) {
				eventFlags |= 1 << EVENT_BTN_HIGHLIGHT;
				onHighlight();
			}
			else {
				eventFlags &= ~(1 << EVENT_BTN_HIGHLIGHT);
				onNormal();
			}
		}

		if (event.type == event.MouseButtonPressed) {
			if (event.mouseButton.button == sf::Mouse::Left) {
				if (isHighlighted()) {
					eventFlags &= ~(1 << EVENT_BTN_RELEASE);
					eventFlags |= 1 << EVENT_BTN_PRESS;
					onClick();
				}
			}
		}

		if (event.type == event.MouseButtonReleased) {
			if (event.mouseButton.button == sf::Mouse::Left) {
				eventFlags |= 1 << EVENT_BTN_RELEASE;
				onRelease();
			}
		}
	}
	else {
		eventFlags = 0;
	}
}