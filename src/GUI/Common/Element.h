#pragma once
#include <SFML/Graphics.hpp>
#include <memory>
#include <iostream>

namespace GUI {
	using namespace std;

	class Element {
	private:
		typedef enum {
			EVENT_BTN_PRESS,
			EVENT_BTN_RELEASE,
			EVENT_BTN_HIGHLIGHT
		};
		sf::Int32 eventFlags;

		shared_ptr <sf::Shape> mListenShape;
	protected:
		sf::RenderWindow &win;
		bool enabled = true;
		bool hidden = false;
	public:
		Element(sf::RenderWindow &render) : win(render) {}
		bool isHighlighted() {return (eventFlags & (1 << EVENT_BTN_HIGHLIGHT));}
		bool isPressed() {return (eventFlags & (1 << EVENT_BTN_PRESS));}
		void setHidden(bool isHidden) {isHidden ? hidden = true : hidden = false;}
		void setEnabled(bool isEnabled) {isEnabled ? enabled = true : enabled = false;}
		void handleEvents(sf::Event &event);
		bool isEnabled() { return enabled; };
		bool isHidden() { return hidden; };

		virtual void onHighlight() = 0;
		virtual void onClick() = 0;
		virtual void onNormal() = 0;
		virtual void onRelease() = 0;
		void setShape(shared_ptr<sf::Shape> shape) {
			mListenShape = shape;
		}

	};
}
