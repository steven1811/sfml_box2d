#pragma once
#include <SFML/Graphics.hpp>
#include <memory>
#include <GUI/Common/Element.h>
#include <iostream>
#define BTN_RECT_COLOR sf::Color::Red
#define BTN_TEXT_COLOR sf::Color::Black
#define BTN_RECT_HIGHLIT_COLOR sf::Color::Magenta
#define BTN_HIGHLIT_VAL 20
namespace GUI{
	using namespace std;
	class Element;

	class Button : public Element  {
	private:

	public:
		shared_ptr<sf::RectangleShape> mRectangle;
		sf::Text mText;
		void scaleButtonToText(sf::Text &text) { mRectangle->setSize(sf::Vector2f(mText.getGlobalBounds().width, mText.getGlobalBounds().height + 10)); }

		Button(sf::RenderWindow &render, sf::Font &font, sf::String text, float posX, float posY);
		void setPosition(float posX, float posY);
		void setText(sf::String str);
		virtual void onNormal();
		virtual void onHighlight();
		virtual void onClick();
		virtual void onRelease();
		void draw();
	};
}
