#include <GUI/Elements/Button.h>

using namespace GUI;

Button::Button(sf::RenderWindow &render, sf::Font &font, sf::String text, float posX, float posY) : Element(render) {
	mRectangle = make_shared<sf::RectangleShape>();
	mText.setFont(font);
	mText.setFillColor(BTN_TEXT_COLOR);
	mText.setCharacterSize(32);
	mText.setString(text);
	mRectangle->setFillColor(BTN_RECT_COLOR);
	mText.setPosition(posX, posY);
	mRectangle->setPosition(posX, posY);
	scaleButtonToText(mText);
	setShape(mRectangle);
}

void Button::setPosition(float posX, float posY) {
	mRectangle->setPosition(sf::Vector2f(posX, posY));
	mText.setPosition(sf::Vector2f(posX, posY));
}

void Button::setText(sf::String str) {
	mText.setString(str);
	scaleButtonToText(mText);
}

void Button::onNormal() {
	mRectangle->setFillColor(BTN_RECT_COLOR);
}

void Button::onHighlight() {
	mRectangle->setFillColor(BTN_RECT_HIGHLIT_COLOR);
}

void Button::onClick() {
	mText.setFillColor(BTN_TEXT_COLOR);
	mRectangle->setFillColor(sf::Color::Cyan);
}
void Button::onRelease() {
	mRectangle->setFillColor(BTN_RECT_COLOR);
}

void Button::draw() {
	if (!hidden) {
		win.draw(*mRectangle);
		win.draw(mText);
	}
}

/*void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	sf::RectangleShape test(sf::Vector2f(100,100));
	test.setFillColor(sf::Color::Red);
	test.setPosition(100, 100);
	//cout << mText.getString().toAnsiString() << endl;
	
	if (!hidden) {
		//target.draw(mRectangle);
		//target.draw(mText);
		//target.draw(test);
		//target.draw(test);
	}
}*/