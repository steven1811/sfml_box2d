#include "GUIMain.h"

void GUIMain::draw() {
	sf::RectangleShape rectangle(sf::Vector2f(win.getDefaultView().getSize().x *0.3f, win.getDefaultView().getSize().y));
	rectangle.setPosition(0.0f, 0.0f);
	rectangle.setFillColor(sf::Color(128, 128, 128, 128));

	if (showSidebar) {
		win.draw(rectangle);
	}

	for (auto it = buttonList.begin(); it != buttonList.end(); ++it) {
		it->draw();
	}
}

void GUIMain::pollEvents(sf::Event &event) {
	for (list<Button>::iterator it = buttonList.begin(); it != buttonList.end(); ++it) {
		it->handleEvents(event);
	}
}

Button& GUIMain::addButton(sf::String text, float posX, float posY) {
	Button btn(win, guiFont, text, posX, posY);
	buttonList.push_back(btn);
	return buttonList.back();
}