#pragma once
#include <Box2D\Box2D.h>

class ShapeFactory {
public:
	void CreateCircle(b2World& World, int posX, int posY, int radius);
	void CreateBox(b2World& World, int MouseX, int MouseY);
};