#include "debugDraw.h"

sf::Color DebugDraw::convertToSFMLColor(const b2Color &color) {
		return sf::Color(color.r*255.f, color.g * 255.f, color.b * 255.f);
}

DebugDraw::DebugDraw(shared_ptr<SharedData> globalData) {
		global = globalData;
		win = global->win;
		world = global->world;

		fpsCounter.setFont(global->mainFont);
		fpsCounter.setFillColor(sf::Color::Black);
		fpsCounter.setCharacterSize(32);
		fpsCounter.setPosition(sf::Vector2f(0, 0));
	}

	void DebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {
		polygon.setOutlineColor(convertToSFMLColor(color));
		polygon.setOutlineThickness(1);
		polygon.setFillColor(sf::Color::Transparent);
		polygon.setPointCount(vertexCount);
		for (int i = 0; i < vertexCount; i++) {
			polygon.setPoint(i, sf::Vector2f(vertices[i].x*SCALE, vertices[i].y*SCALE));
		}
		win->draw(polygon);
	}

	void DebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {
		polygon.setFillColor(convertToSFMLColor(color));
		polygon.setPointCount(vertexCount);
		for (int i = 0; i < vertexCount; i++) {
			polygon.setPoint(i, sf::Vector2f(vertices[i].x*SCALE, vertices[i].y*SCALE));
		}
		win->draw(polygon);
	}

	void DebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color) {
		circle.setRadius(radius*SCALE);
		circle.setOutlineThickness(1);
		circle.setOutlineColor(convertToSFMLColor(color));
		circle.setFillColor(sf::Color::Transparent);
		circle.setPosition(sf::Vector2f(center.x * SCALE, center.y * SCALE));
		win->draw(circle);
	}

	void DebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color) {
		circle.setRadius(radius*SCALE);
		circle.setOutlineThickness(1.0f);
		circle.setOutlineColor(sf::Color::Black);
		circle.setFillColor(convertToSFMLColor(color));
		circle.setPosition(sf::Vector2f(center.x * SCALE, center.y * SCALE));
		circle.setOrigin(sf::Vector2f(radius*SCALE, radius*SCALE));
		win->draw(circle);
	}

	void DebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) {
		sf::Vertex line[] =
		{
			sf::Vertex(sf::Vector2f(p1.x * SCALE, p1.y* SCALE), convertToSFMLColor(color)),
			sf::Vertex(sf::Vector2f(p2.x * SCALE, p2.y* SCALE), convertToSFMLColor(color))
		};

		win->draw(line, 2, sf::Lines);
	}

	void DebugDraw::DrawTransform(const b2Transform& xf) {
		sf::RectangleShape line(sf::Vector2f(15.0f, 1.0f));
		line.setPosition(xf.p.x * SCALE, xf.p.y * SCALE);
		line.rotate(180 / b2_pi * xf.q.GetAngle());
		win->draw(line);
		return;
	}

	void DebugDraw::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color) {
		circle.setRadius(size*SCALE);
		circle.setPosition(sf::Vector2f(p.x * SCALE, p.y * SCALE));
		circle.setFillColor(convertToSFMLColor(color));
		win->draw(circle);
	}

	void DebugDraw::DrawOverlay() {
		if (overlayEnabled) {
			if (overlayRefreshClock.getElapsedTime().asMilliseconds() > overlayRefreshRate_ms) {
				overlayRefreshClock.restart();

				//FPS Counter
				std::stringstream stream;
				stream << "FPS:" << global->fps;
				fpsCounter.setString(stream.str());

				//Console
				if (consoleEnabled) {
					//Pause Game
				}
			}
			win->draw(fpsCounter);
			

		}
	}

	void DebugDraw::ToggleOverlay() {
		overlayEnabled = !overlayEnabled;
	}

	void DebugDraw::ToggleConsole() {
		consoleEnabled = !consoleEnabled;
	}